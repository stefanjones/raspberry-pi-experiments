#!/usr/bin/python
#
# Dashboard for Raspberry Pi to show weather radar
#
#
import datetime
import math
import requests
from string import zfill
from StringIO import StringIO

import Tkinter
import Image,ImageTk,ImageOps,ImageFilter,ImageDraw,ImageFont

update_frequency = 600 #seconds

def request_radar(city):
  r = request_last_radar_image(city)
  return r

def request_overlay(city):
  r = requests.get("http://weather.gc.ca/cacheable/images/radar/layers/default_cities/"+city.lower()+"_towns.gif?ver=1375736327")
  return r

def request_last_radar_image(city):
  now = datetime.datetime.utcnow()
  # We need the previously passed minute on the tens
  minutes = now.strftime("%M")
  hours = int(now.strftime("%H"))
  last_minute = int(math.floor(float(minutes) * 0.1)*10)-10
  
  # TODO: Work on day boundaries
  if last_minute < 0:
    last_minute = 60 + last_minute
    hours = hours - 1
  else:
    print "Adjusted minute is greater than zero: "+minutes

  last_hour = zfill(hours, 2)
  last_minute = zfill(last_minute, 2)
  
  prefix_time = now.strftime("%Y_%m_%d_")
  r = requests.get('http://weather.gc.ca/data/radar/temp_image/'+city+'/'+city+'_PRECIP_RAIN_'+prefix_time+str(last_hour)+"_"+str(last_minute)+".GIF")
  return r


def merge_radar_images(r, overlay_r, win_x, win_y):

  # verify 
  if r.status_code == 200:
    radar = Image.open(StringIO(r.content))
    overlay_radar = Image.open(StringIO(overlay_r.content))
    
    if overlay_r.status_code == 200:
      mask = Image.open(StringIO(overlay_r.content)).convert('L').filter(ImageFilter.MinFilter(3))
      mask = ImageOps.invert(mask)
      radar.paste(overlay_radar, (0,0), mask)
    else:
      print "404 on mask at "+overlay_r.url

    radar = radar.crop((100,100,radar.size[0]-100,radar.size[1]))
    
    # Scale image  to fit vertical dimension
    im_x = radar.size[0]
    im_y = radar.size[1]
    ratio = im_x / im_y
    radar = radar.resize((win_y * ratio, win_y), Image.LINEAR)

    # Add label
    draw = ImageDraw.Draw(radar)
    font = ImageFont.truetype("/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf", 26)
    file_text = r.url.rpartition("/")[2]
    draw.text((10, 10), file_text, font=font)
    del draw

    photo_image = ImageTk.PhotoImage(radar)
    return photo_image
  else: 
    print "404 on "+r.url
    return

# GUI
root = Tkinter.Tk()

# Fullscreen Mode
win_x = root.winfo_screenwidth()
win_y = root.winfo_screenheight()
root.geometry(("%dx%d")%(win_x,win_y))
root.title(" ")
root.configure(background='black')
button = Tkinter.Button(root, text="No image available",command=root.destroy)
button.pack()

# No need to request overlay every time  
overlay_r = request_overlay("XRA")  

def update_button():
  print "Updating button"
  r = request_radar("XRA")
  photo_image = merge_radar_images(r, overlay_r, win_x, win_y)
  button.image = photo_image # keep a reference for tkinter
  button.config(image=photo_image)
  
def wait_and_update():
  print "Will request a new update in "+str(update_frequency)+" seconds"
  root.after((update_frequency*1000), update_button)
  root.after((update_frequency*1000), wait_and_update)

update_button()
root.after(1000, wait_and_update)
root.mainloop()