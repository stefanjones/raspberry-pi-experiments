#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import subprocess
import os

ttl = 3600
PIR = 23
pirState = True                        # we start, assuming no motion detected
pirVal = True                          # we start, assuming no motion detected

command = "/home/pi/screen_control.sh"
os.environ["DISPLAY"] = ":0"
 
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIR, GPIO.IN)

while True:
    pirVal = GPIO.input(PIR)            # read input value
    if (pirVal == True):                # check if the input is HIGH
        if (pirState == False):
            # we have _just_ turned on
            pirState = True
            subprocess.Popen(command+" on",shell=True)
            time.sleep(ttl)
    else:
        if (pirState == True):
            # we have _just_ turned off
            pirState = False
            subprocess.Popen(command+" off",shell=True)
            time.sleep(1)
