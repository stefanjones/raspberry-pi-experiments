#!/bin/bash

# Enable and disable composite video on a raspberry pi.
# Accepts 'on' and 'off' parameters. Suitable for cron jobs.
# Assumes composite is set to NTSC with a 16:9 display

if [ -e $1 ]; then
  echo "Please pass a parameter of 'off' or 'on' to continue."
  exit 1
fi

if [ $1 = "off" ]; then
  echo "Turning screen off"
  /opt/vc/bin/tvservice -o

else
  echo "Turning screen on"
  /opt/vc/bin/tvservice -p
  /opt/vc/bin/tvservice -c "NTSC 16:9"
  fbset -depth 8 && fbset -depth 16
  sudo -u pi xrefresh
fi
